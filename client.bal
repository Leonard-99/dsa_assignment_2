import ballerina/http;
import ballerina/kafka;
import ballerina/io;
import ballerina/log;
import ballerina/mime;

// Constants to store client credentials
final string CLIENT_USERNAME = "Client";
final string CLIENT_PASSWORD = "Client";

// Kafka producer configurations
kafka:ProducerConfig producerConfigs = {
    // Here we create a producer configs with optional parameters client.id - used for broker side logging.
    bootstrapServers: "localhost:9096",
    clientID: "file-storage-producer",
    // number of acknowledgments for request complete
    acks: "all",
     // number of retries if record send fails.
    noRetries: 3,
    valueSerializerType: kafka:SER_FIL,
    schemaRegistryUrl: "http://localhost:8081"
};

kafka:Producer kafkaProducer = new(producerConfigs);

// HTTP service endpoint
listener http:Listener httpListener = new(9091);

@http:ServiceConfig { basePath: "/file" }
service clientService on httpListener {


    @http:ResourceConfig { methods: ["POST"], consumes: ["application/json"], produces: ["application/json"] }
    // write operation
    resource function writeFile(http:Caller caller, http:Request request, json content, string path) {
        http:Response response = new;

       io:println(" writting to the file :");
       io:ByteChannel byteChannel = io:openFile(path, io:WRITE);
       io:CharacterChannel ch = new io:CharacterChannel(byteChannel, "UTF8")

       match ch.writeJson(content) {
           error err => {
               close(ch);
               throw err;
           }
            () => {
                close(ch);
                io:println("Content written successfully");
            }
        }
    
        // Initialise the file path
        string path = "./distributedStorageSyatem/files.txt";
        
        // reads the file content as a byte array
        byte[] bytes = check io:fileReadBytes(path);
        
        // Hashing bytes value using the MD5 hashing algorithm
        // Printing the value using the Hex encoding
        byte[] output = crypto:hashMd5(bytes);
        io:println("Hex encoded hash with MD5: " + output.toBase16());

        // writes file to the given destination
        check io:fileWriteBytes(fileCopyPath1, bytes);
        io:println("Successfully copied the file as a byte array.");

        // overridde the default size by the value of 30 MB
        stream<io:Block, io:Error?> blockStream = check
        io:fileReadBlocksAsStream(path, 31457280);

        // writting content to the given destination using the given stream
        check io:fileWriteBlocksFromStream(fileCopyPath2, blockStream);
        io:println("Successfully copied the file as a stream.");

        json|error reqPayload = request.getJsonPayload();

        if (reqPayload is error) {
            response.statusCode = 400;
            response.setJsonPayload({ "Message": "Invalid payload - Not a valid JSON payload" });
            var result = caller->respond(response);
        } else {
            json|error username = reqPayload.Username;
            json|error password = reqPayload.Password;
            json|error path = reqPayload.path;
            json|error size = reqPayload.size;

            // If payload parsing fails, it send a "Bad Request" message as the response
            if (username is error || password is error || path is error || size is error) {
                response.statusCode = 400;
                response.setJsonPayload({ "Message": "Bad Request: Invalid payload" });
                var responseResult = caller->respond(response);
            } else {
		// Convert the file size value to byte value
		var result = byte:fromString(size.toString());
		if (result is error) {
		    response.statusCode = 400;
		    response.setJsonPayload({ "Message": " File too large than 30 MB" });
		    var responseResult = caller->respond(response);
		} else {
		    newFileSize = result;
		}

		// If the credentials does not match with the client credentials,
		// send an "Access Forbidden" response message
		if (username.toString() != CLIENT_USERNAME || password.toString() != CLIENT_PASSWORD) {
		    response.statusCode = 403;
		    response.setJsonPayload({ "Message": "Access Forbidden" });
		    var responseResult = caller->respond(response);
		}

		// Construct and serialise the message to be published to the Kafka topic
		json fileWriteInfo = { "File": path, "size": newSize };
		byte[] serialisedMsg = fileWriteInfo.toString().toByteArray("UTF-8");

		// Produce the message and publish it to the Kafka topic
		var sendResult = kafkaProducer->send(serialisedMsg, "book", partition = 1);
		// Send internal server error if the sending has failed
		if (sendResult is error) {
		    response.statusCode = 500;
		    response.setJsonPayload({ "Message": "Kafka producer failed to send data" });
		    var responseResult = caller->respond(response);
		}
		// Send a success status to the client request
		response.setJsonPayload({ "Status": "Success" });
		var responseResult = caller->respond(response);
	    }
        }
    }
    public type FilestorageService record{
    string name;
    string path;
    byte[] size;
    };
    string schema = "{\"type\" : \"record\"," +
                  "\"namespace\" : \"Sacky\"," +
                  "\"name\" : \"file\"," +
                  "\"fields\" : [" + 
                    "{ \"name\" : \"name\", \"type\" : \"string\" }," +
                    "{ \"name\" : \"path\", \"type\" : \"string\" }," +
                    "{ \"name\" : \"size\", \"type\" : \" byte\" }," +
                    "]}";
                    
    @http:ResourceConfig { methods: ["GET"], consumes: ["application/json"], produces: ["application/json"] }
    // read operation
    resource function readFile(http:Caller caller, http:Request request, string path) returns json {
        http:Response response = new;                
    // Create a byte channel from the given file path
    io:ByteChannel byteChannel = io:openFile(path, io:READ);
    // Derive the character channel from the byte channel
    io:CharacterChannel ch = new io:CharacterChannel(byteChannel, "UTF8");
    // This is how json content is read from the character channel
    match ch.readJson() {
        json result => {
            close(ch);
            return result;
        }
        error err => {
            close(ch);
            throw err;
        }
    }

    function handleFileContent(mime:Entity fileContent) returns string {
        mime:MediaType mediaType = check mime:getMediaType(fileContent.getContentType());
        string baseType = mediaType.getBaseType();
        byte[] size = check fileContent.getByteArray();
        string base64 = mime:byteArrayToString(mime:base64EncodeByteArray(size), "UTF8");
        return base64;
    }
    function insertTofileStorageService(string name, string path, byte[] size) returns boolean {
    log:printInfo("Inserting file to fileStorageService: ");
    boolean success = false;

    transaction with retries = 3, oncommit = onCommitFunction, onabort = onAbortFunction {
        var result = fileStorageServiceEP->write(REQUEST_TO_INSERT_FILE_TO_FILESTORAGESERVICE, name, path);

        match result {
            int c => {
                if (c < 0) {
                    log:printError("Unable to insert File into fileStorageService record:");
                } else {
                    log:printInfo("Successful! Inserted Into fileStorageService record: ");
                    success = true;
                }
            }
            error err => {
                log:printError(err.message);
            }
        }
    } onretry {
        io:println("Retrying transaction");
    }
    return success;
    }

    function setErrorResponse(http:Response response, error e, int statusCode) {
        response.statusCode = statusCode;
        response.reasonPhrase = e.message;
        response.setJsonPayload(check <json>e);
    }
    
    function onCommitFunction(string transactionId) {
        io:println("Transaction: " + transactionId + " committed");
    }

    function onAbortFunction(string transactionId) {
        io:println("Transaction: " + transactionId + " aborted");
    }

    function main (string...args) {
        string path = "./C:/Users/Sacky/Desktop/distributedStorageSystem";
        Json fileData = {"name":"",
                   "size":""}
    };
    int count = 0;
    json []allSt;
    while (count <= 1){
    json temp;
    temp.name =
    ){

    }
    fileData.name = io:readln("Enter your file file Name");
    fileData.size = io:readln("Enter your file size");
    
    io:println("Preparing to write json file");
    write(fileData, path);

    io:println("Preparing to read the content written");
    json content = read(path);
    io:println(content);

    kafka:FileStorageRecord fileStorageRecord = {
        schemaString: schema,
        dataRecord: file
    };

    var result = producer->send(fileStorageRecord, "add-file-with-name");
    if (result is kafka:ProducerError) {
        io:println(result);
    } else {
        io:println("Successfuly subscribed");
    }
}
