import ballerina/kafka;
import ballerina/internal;
import ballerina/io;
import ballerina/log;

@kubernetes:Deployment {
    image:"distributed-storage-service",
    name:"kafka-distributed-storage-service"
}

endpoint kafka:consumerConfig consumer1 {
  bootstrapServers:"localhost:9092",
    groupId:"student",
    topics:[" dsa book"],
    pollingInterval:1000,
    autoCommit:false
};

listener kafka:Listener cons new (consumer1)

service<kafka:Consumer> kafkaService bind consumer1 {

    resource function onMessage(kafka:ConsumerAction consumerAction, kafka:ConsumerRecord[] records) {
        // Dispatched set of Kafka records to service
        // process each one by one
        foreach kafkaRecord in records {
            processKafkaRecord(kafkaRecord);
        }
        // Commit offsets returned for returned records, marking them as consumed.
        var consumerAction = kafkaConsumer->commit();
		if (consumerAction is error) {
            log:printError("Error occurred while committing the " +
                "offsets for the consumer ", err = consumerAction);
    }

    function processKafkaRecord(kafka:ConsumerRecord kafkaRecord) {
        byte[] serializedMsg = kafkaRecord.value;
        string msg = internal:byteArrayToString(serializedMsg,"UTF-8");
        json fileJson = msg;
        io:println(fileJson[0]);
        io:println("New message received from the client");
        // Print the retrieved Kafka record.
        io:println("Topic: " + kafkaRecord.topic + " Received Message: " + msg);
        io:println("storageService has been written with the new File");
        io:println(distributed-fileStorage.NAME.toString()+"\t"+distributed-fileStorage.PATH.toString()+"\t"+distributedfileStorage.SIZE.toString());
    }

}

