import ballerina/kafka;
import ballerina/io;
import ballerina/h2;
import ballerina/mysql;

endpoint kafka:ControlProducer kafkaProducer {

    bootstrapServers:"localhost:9092",
    clientID:"file_creation",
    acks:"all",
    noRetries:3
};
//Endpoit for H2 Database
endpoint h2:Client testDB {
    path: "./test",
    filePath: "testDB",
    username: "ADMIN",
    password: "",
    poolOptions: { maximumPoolSize: 3 }
};
function main (string... args) {
    io:println("Creating a distributedFile:");
    var ret = testDB->write("CREATE TABLE DISTRIBUTEDFILE(FILENAME VARCHAR,
                     FILEPATH VARCHAR(255),FILESIZE BYTE, PRIMARY KEY (FILENAME))");
    handleWriteFile(ret, "Create distributedFile table");

    io:println("\nThe write operation - Inserting data to a distributedFile");
    ret = testDB->write("INSERT INTO distributedFile(fileName, filePath,fileSize)
                          values ('kafka.txt,'C:/Users/Sacky/Desktop/assignment02','17MB')");
    handleWriteFile(ret, "Insert to distributedFile with no parameters");
    
    //Prompt user for a file details and insert into the distributedFile
    string filePath = io:readln("Enter filePath:");
    var  fileSize = <byte>io:readln("Enter file size:");
    byte resFlSize;
    match fileSize{
        byte resByte=>{
            resFlSize = resByte;
        }
        error e=>io:println("error:"+e.message);
    }

    var writeFl = testDB->write("INSERT INTO distributedFile(fileName, filePath,fileSize) values ("+resFlSize+",'"+filePath+"', 35)");
     handleWriteFile(writeFl, "Create distributedFile");

    //get items in distributedFile and send
    io:println("\nThe select operation - Select data from a distributedFile");
    var selectRet = testDB->select("SELECT * FROM distributedFile", ());
    distributedFile dt;
    match selectRet {
        distributedFile tableReturned => dt = tableReturned;
        error e => io:println("Select data from distributedFile distributedFile failed: "
                              + e.message);
    }
    //convert distributedFile to json and send to server
    io:println("\nConvert the distributedFile into json");
    var jsonConversionRet = <json>dt;
    json tableMsg;
    match jsonConversionRet {
        json jsonRes => {
            io:print("JSON: ");
            tableMsg = jsonRes;
            io:println(io:sprintf("%s", tableMsg));
        }
        error e => io:println("Error in distributedFile to json conversion");
    }
    io:println("TABLE READY FOR SENDING:");
    io:println(io:sprintf("%s", tableMsg));

    byte[] serializedMsg = tableMsg.toString().toByteArray("UTF-8");
    kafkaProducer->send(serializedMsg, "DSA", partition = 0);
    io:println("message sent to kafka...");

}
function handleWriteFile(int|error returned, string message) {
    match returned {
        int retInt => io:println(message + " status: " + retInt);
        error e => io:println(message + " failed: " + e.message);
    }
}